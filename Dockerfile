FROM python:alpine

RUN apk add --update make \
  postgresql-dev gcc python3-dev musl-dev libffi-dev openssl-dev cargo

RUN pip install --upgrade pip
RUN pip install sphinx sphinx_rtd_theme recommonmark \
  sphinx-autobuild
COPY . /app

WORKDIR /app

RUN pip install -r ./back-office/libshare/requirements.txt

RUN rm -rf /app/front/node_modules /app/_build/** /app/**/__pycache__ /app/**/.pytest_cache

EXPOSE 8181

CMD [ "make", "livehtml" ]
