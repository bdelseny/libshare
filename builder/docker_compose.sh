#!/bin/sh

set -x

docker compose down --remove-orphans --rmi all -v
docker compose rm -f -s -v
docker image prune -af
docker compose up --force-recreate --build -d
docker image prune -af
