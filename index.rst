.. LibShare documentation master file, created by
   sphinx-quickstart on Tue Apr 13 10:52:45 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to LibShare's documentation!
====================================

.. toctree::
   :maxdepth: 2

   README
   back-office/index
   database/index
   builder/index
   doc/LICENSE

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
