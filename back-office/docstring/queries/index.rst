SQL queries
===========

.. automodule:: libshare.queries.contact
    :members:

----

.. automodule:: libshare.queries.media
    :members:

----

.. automodule:: libshare.queries.user
    :members:
