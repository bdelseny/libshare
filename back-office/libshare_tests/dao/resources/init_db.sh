#!/bin/sh

CURRENT_PATH=$(dirname $0)

PGPASSWORD=${DB_PASSWORD} psql -h ${DB_URL} -U "${DB_USER}" -d "${DB_NAME}" -a -f "${CURRENT_PATH}/init_db.sql"
