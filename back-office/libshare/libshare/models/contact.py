"""Contact model module"""
from dataclasses import dataclass


@dataclass
class Contact():
    """Contact model

    Attributes
    ----------
    id : int, optional
        contact unique id, by default None
    first_name : str
        contact first name
    last_name : str
        contact last name
    email : str, optional
        contact email address, by default None
    phone : str, optional
        contact phone number, by default None
    information : str, optional
        contact additional information, by default None
    """

    first_name: str
    last_name: str
    user_id: int
    id: int = 0  # pylint: disable=invalid-name
    email: str = ''
    phone: str = ''
    information: str = ''

    @classmethod
    def init_from_dict(cls, data: dict):
        """Init contact from unordered contact information

        :param data: unordered contact information
        :type data: dict
        :return: contact
        :rtype: _type_
        """
        new_contact = cls(data['first_name'],
                          data['last_name'], data['user_id'])
        new_contact.__dict__.update(data)
        return new_contact
