"""Media model module"""
from dataclasses import dataclass


@dataclass
class Media():
    """Media model

    Attributes
    ----------
    id : int, optional
        media unique id, by default None
    userid : int
        user unique id
    isbn : str
        media isbn
    title : str
        media title
    authors : str
        media authors
    shared_id : int, optional
        contact id, by default None
    mediaabstract : str, optional
        media abstract, by default None
    numberofmedia : int, optional
        number of media, by default 1
    cover : str, optional
        cover url, by default None
    """
    #pylint: disable=too-many-instance-attributes

    userid: int
    isbn: str
    title: str
    authors: str
    id: int = 0  # pylint: disable=invalid-name
    mediaabstract: str = ''
    numberofmedia: int = 1
    cover: str = ''
    shared_id: None = None

    @classmethod
    def init_from_dict(cls, data: dict):
        """Init media from unordered media information

        :param data: unordered media information
        :type data: dict
        :return: media
        :rtype: _type_
        """
        new_media = cls(data['userid'], data['isbn'],
                        data['title'], data['authors'])
        new_media.__dict__.update(data)
        return new_media
