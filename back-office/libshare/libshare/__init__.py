"""Libshare API controller"""
import os
import logging
from flask import Flask
from flask_cors import CORS
from flask_jwt_extended import JWTManager


logging.basicConfig(level=logging.DEBUG)


def create_app(config_filename):
# pylint: disable=unused-argument
    """Create Libshare application

    :param config_filename: configuration file of the app
    :type config_filename: str
    :return: libshare application
    :rtype: Flask
    """
    # pylint: disable=import-outside-toplevel
    app = Flask(__name__)
    # pylint: disable=unused-variable
    cors = CORS(app)
    app.config['CORS_HEADERS'] = 'Content-Type'
    # Setup the Flask-JWT-Extended extension
    app.config['JWT_SECRET_KEY'] = os.environ.get('JWT_KEY')
    # pylint: disable=unused-variable
    jwt = JWTManager(app)

    # pylint:disable=wrong-import-position
    from libshare.controllers.controller import main
    app.register_blueprint(main)
    from libshare.controllers.user import userAPI
    app.register_blueprint(userAPI)
    from libshare.controllers.media import media
    app.register_blueprint(media)
    from libshare.controllers.contact import contact
    app.register_blueprint(contact)

    if __name__ == '__main__':
        app.run(host='0.0.0.0')

    return app
