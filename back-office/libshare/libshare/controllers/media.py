"""Media API"""
from flask import request, jsonify, Blueprint
from flask_cors import cross_origin
from flask_jwt_extended import get_jwt_identity, jwt_required

import libshare.services.media as service

media = Blueprint('media', __name__, url_prefix='')

@media.route('/documents', methods=['GET'])
@jwt_required()
@cross_origin()
def get_documents():
    """Get all user medias.

    Returns
    -------
    response
        list of media

    Notes
    -----
    **Route**
        - /documents
    **Method**
        - GET
    **Authentication**
        - Required
    **Response**
        - 200, JSON:
            .. highlight:: JSON
            .. code-block:: JSON

                [{
                    "id": 0,
                    "title": "string",
                    "authors": "string",
                    "isbn": "string",
                    "numberofmedia": 0,
                    "shared": true,
                    "abstract": "string",
                    "cover": "string",
                    "userid": 0
                },{},{}]

    """
    return jsonify(service.get(get_jwt_identity()))

@media.route('/documents/<media_id>', methods=['GET'])
@jwt_required()
@cross_origin()
def get_document_by_id(media_id: int):
    """Get a media from its user and id

    Parameters
    ----------
    media_id : int
        media unique id

    Returns
    -------
    response
        a media

    Notes
    -----
    **Route**
        - /documents/<media_id>
    **Method**
        - GET
    **Parameter**
        - media_id as an integer
    **Response**
        - 200, JSON:
            .. highlight:: JSON
            .. code-block:: JSON

                {
                    "id": 0,
                    "title": "string",
                    "authors": "string",
                    "isbn": "string",
                    "numberofmedia": 0,
                    "shared": true,
                    "abstract": "string",
                    "cover": "string",
                    "userid": 0
                }

    """
    return jsonify(service.get(get_jwt_identity(), media_id))


@media.route('/addDocument', methods=['PUT'])
@jwt_required()
@cross_origin()
def add_document():
    """Add a media to the user media list

    Returns
    -------
    response
        the media added to the list

    Notes
    -----
    **Route**
        - /addDocuments
    **Method**
        - PUT
    **Request**
        - JSON:
            .. highlight:: JSON
            .. code-block:: JSON

                {
                    "id": 0,
                    "title": "string",
                    "authors": "string",
                    "isbn": "string",
                    "numberofmedia": 0,
                    "shared": true,
                    "abstract": "string",
                    "cover": "string",
                    "userid": 0
                }

    **Response**
        - 200, JSON:
            .. highlight:: JSON
            .. code-block:: JSON

                {
                    "id": 0,
                    "title": "string",
                    "authors": "string",
                    "isbn": "string",
                    "numberofmedia": 0,
                    "shared": true,
                    "abstract": "string",
                    "cover": "string",
                    "userid": 0
                }

    """
    return jsonify(service.add(get_jwt_identity(), request.json))


@media.route('/updateDocument', methods=['POST'])
@jwt_required()
@cross_origin()
def update_document():
    """Update a media information

    Returns
    -------
    response
        the updated media

    Notes
    -----
    **Route**
        - /updateDocument
    **Method**
        - POST
    **Request**
        - JSON:
            .. highlight:: JSON
            .. code-block:: JSON

                {
                    "id": 0,
                    "title": "string",
                    "authors": "string",
                    "isbn": "string",
                    "numberofmedia": 0,
                    "shared": true,
                    "abstract": "string",
                    "cover": "string",
                    "userid": 0
                }

    **Response**
        - 200, JSON:
            .. highlight:: JSON
            .. code-block:: JSON

                {
                    "id": 0,
                    "title": "string",
                    "authors": "string",
                    "isbn": "string",
                    "numberofmedia": 0,
                    "shared": true,
                    "abstract": "string",
                    "cover": "string",
                    "userid": 0
                }

    """
    return jsonify(service.update(get_jwt_identity(), request.json))


@media.route('/delete/<media_id>', methods=['DELETE'])
@jwt_required()
@cross_origin()
def delete_document(media_id: int):
    """Delete a media

    Parameters
    ----------
    media_id : int
        media id to delete

    Returns
    -------
    response
        a success message containing information \
            about the deleted media

    Notes
    -----
    **Route**
        - /delete/<media_id>
    **Method**
        - DELETE
    **Parameter**
        - media_id as an integer
    **Response**
        - 200, a success message with media information
    """
    return jsonify(service.delete(get_jwt_identity(), media_id))
