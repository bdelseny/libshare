"""Contact database queries"""

#: Get a contact from its ID and user ID
GET = ("SELECT * "
                "FROM public.contact "
                "WHERE id=%(id)s AND user_id=%(user_id)s;")

#: Get all user contact
GET_ALL = ("SELECT * "
                "FROM public.contact "
                "WHERE user_id=%(user_id)s;")

#: Get a contact from its user ID and one other information
GET_BY_QUERY = ("SELECT * "
                "FROM public.contact "
                "WHERE user_id=%(user_id)s "
                "AND (first_name LIKE %(query)s OR last_name LIKE %(query)s OR email LIKE %(query)s "
                "OR phone LIKE %(query)s OR information LIKE %(query)s);")

#: Add a new contact
ADD = ("INSERT INTO public.contact "
                "(first_name,last_name,email,phone,information,user_id) "
                "VALUES(%(first_name)s,%(last_name)s,%(email)s,%(phone)s,%(information)s,%(user_id)s) "
                "RETURNING *;")

#: Update contact information
UPDATE = ("UPDATE public.contact "
                "SET first_name=%(first_name)s, last_name=%(last_name)s, email=%(email)s, "
                "phone=%(phone)s, information=%(information)s, "
                "user_id=%(user_id)s "
                "WHERE id=%(id)s AND user_id=%(user_id)s "
                "RETURNING *;")

#: Remove a contact
DELETE = ("DELETE FROM public.contact "
                "WHERE id=%(id)s AND user_id=%(user_id)s;")
