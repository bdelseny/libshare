"""User service"""

import libshare.dao.user as dao
from libshare.models.user import User


def get(email: str) -> User:
    """Get a User from its email address

    :param email: user email
    :type email: str
    :return: user with provided email address
    :rtype: User
    """
    return User.init_from_dict(dao.get_by_email(email))


def add(user: dict) -> User:
    """Save User information

    :param user: user to save
    :type user: dict
    :return: save user
    :rtype: User
    """
    return User.init_from_dict(dao.add(User.init_from_dict(user)))
