"""Media DAO"""

import libshare.queries.media as queries
from libshare.models.media import Media
from libshare.dao.connection import execute_statement

def get_all(user_id: int) -> list[dict]:
    """Get all user medias

    Parameters
    ----------
    user_id : int
        User ID

    Returns
    -------
    list[dict]
        list of medias
    """
    return execute_statement(queries.GET_ALL, {'user_id': user_id}, return_list=True)


def add(media: Media) -> dict:
    """Add a media

    Parameters
    ----------
    media : dict
        Media to add to list of user medias

    Returns
    -------
    dict
        a media
    """
    return execute_statement(queries.ADD, vars(media))


def get_by_id(media_info: tuple) -> dict:
    """Get a media by its ID

    Parameters
    ----------
    media_info : tuple
        Tuple with media ID and user ID

    Returns
    -------
    dict
        a media
    """
    return execute_statement(queries.GET, {'media_id': media_info[0], 'user_id': media_info[1]})


def update(media: Media) -> dict:
    """Update a media

    Parameters
    ----------
    media : dict
        Media to update

    Returns
    -------
    dict
        a media
    """
    return execute_statement(queries.UPDATE, vars(media))


def delete(media: Media) -> dict:
    """Delete a media

    Parameters
    ----------
    media : tuple
        Tuple with media ID and user ID
    """
    return execute_statement(queries.DELETE, vars(media), to_delete=True)
