"""User DAO"""

import libshare.queries.user as queries
from libshare.models.user import User
from libshare.dao.connection import execute_statement


def add(user: User) -> dict:
    """Add a user to the database

    Parameters
    ----------
    user : User
        User to add to database

    Returns
    -------
    dict
        Added user
    """
    return execute_statement(queries.ADD, vars(user))


def get_by_email(email: str) -> dict:
    """Get a user from database based on its email

    Parameters
    ----------
    email : str
        User email

    Returns
    -------
    dict
        User found in database
    """
    return execute_statement(queries.GET_BY_EMAIL, {'email': email})
