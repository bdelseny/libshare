FROM python:alpine

WORKDIR /libshare

COPY . /libshare

RUN apk add postgresql-dev gcc python3-dev musl-dev libffi-dev openssl-dev cargo postgresql-client

RUN pip install -r libshare/requirements.txt

RUN pip install pytest pytest-cov pytest-html coverage pylint pylint-json2html

RUN pip install -e libshare

CMD sh
