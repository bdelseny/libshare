# BackOffice

LibShare back-end repository


## Development guide

*Work in progress*

Go to the repository:
```sh
cd back-office
```

Build docker image using the development dockerfile:
```sh
docker build -f Dev.Dockerfile -t libshare_bo .
```
You may need to launch docker commands as a sudo user if your current user is not on the docker group.
- ```-f Dev.Dockerfile``` argument is used to build the docker image using the development configuration which allow tracing, debugging and live reload. For live reload you will also need to correctly set the ```docker run``` command.
- ```-t libshare_bo``` is used to tag the image, you can name it as you need.

Run the docker image:
```sh
docker run --mount type=bind,source="$(pwd)"/app,target=/app -p 5000:5000 libshare_bo
```
- ```--mount type=bind,source="$(pwd)"/app,target=/app``` is used to bind, ```type=bind```, our local ```app``` development folder, ```source="$(pwd)"/app```, to the docker image ```app``` folder, ```target=/app```. You need to set this argument to allow live reload when changing local development file.
- ```-p 5000:5000``` is used to bind the docker port to your local host port allowing you to open the application on your host at http://localhost:5000/

## Run pytest

- Build the image:

    ```sh
    docker compose -f ./tests.docker-compose.yml up --force-recreate --build -d
    ```

- Launch docker and enter it:

    ```sh
    docker exec -it libshare_backend_test sh
    ```

- Run linter:

    ```sh
    pylint libshare/libshare
    pylint-json2html -o pylint.html pylint.json
    ```

- Run tester:

    ```sh
    pytest --cov=libshare/ --cov-fail-under=80 --cov-report=html:./coverage --html=test_report.html --self-contained-html --verbose -r A --full-trace
    ```

## Curl request

Set the https URL:

```bash
URL=https://my.url
```

Base command:

```bash
curl -X GET -k -i "${URL}/path"
curl -X POST -k -H 'Content-Type: application/json' -i "${URL}/path" --data '{
    "key": "value",
    "key": "value",
    "key": "value"
}'
```

### Example:

Authenticate, get token, get document list

```bash
curl -X POST -k -H 'Content-Type: application/json' "${URL}/register" --data '{
    "email": "test@test.test",
    "password": "test",
    "username": "test"
}'
json=$(curl -X POST -k -H 'Content-Type: application/json' "${URL}/authenticate" --data '{
    "email": "test@test.test",
    "password": "test"
}')
token=$( jq -r ".token" <<<"$json" )
curl -X GET -k -H 'Content-Type: application/json' -H "Authorization: Bearer ${token}" "${URL}/documents"
```
