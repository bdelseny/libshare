Database
========

.. image:: schema.drawio.svg


Users
-----

`Users` table containing user information.

- `id`: (`serial4`, `NOT NULL`, `PRIMARY_KEY`) a unique id for the user
- `username`: (`varchar(250)`, `NOT NULL`, `UNIQUE`) user name or pseudo
- `email`: (`varchar(250)`, `NOT NULL`, `UNIQUE`) user email adress
- `userpassword`: (`varchar(250)`, `NOT NULL`) user password (should be encrypted by application)


Media
-----

`Media` table containing media information.

- `id`: (`serial4`, `NOT NULL`, `PRIMARY_KEY`) a unique id for the media
- `isbn`: (`varchar(250)`) media ISBN
- `title`: (`varchar(250)`, `NOT NULL`) media title
- `authors`: (`varchar(250)`) media authors
- `mediaabstract`: (`text`) media abstract
- `cover`: (`text`) media cover, an URL to an image available throuh HTTP (HTTPS recommanded)
- `numberofmedia`: (`varchar(250)`) media number of copies
- `shared`: (`bool`, `NOT NULL`, `DEFAULT` => `false`) `true` if the media is shared
- `userid`: (`int4`, `NOT NULL`, `FOREIGN_KEY` => `id` from `users` table) user id who owned the media


Information
===========

For now the `media` table is aiming to receive books information.
It is meant to change to support all kind of media.
It is also meant to let the administrator or users to custom the media fields.
Issues/story may not have been writen on these topic for the moment.
