DO $$BEGIN IF EXISTS(
    SELECT *
    FROM information_schema.columns
    WHERE table_name = 'media'
        AND column_name = 'author'
) THEN
ALTER TABLE public.media
    RENAME COLUMN author TO authors;
END IF;
END $$;