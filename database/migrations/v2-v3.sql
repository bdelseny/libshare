DO $$BEGIN IF EXISTS(
    SELECT *
    FROM information_schema.columns
    WHERE table_name = 'media'
        AND column_name = 'shared'
) THEN
ALTER TABLE public.media DROP shared;
ALTER TABLE public.media
ADD shared_id INTEGER;
END IF;
END $$;
DO $$BEGIN IF NOT EXISTS(
    SELECT *
    FROM information_schema.tables
    WHERE table_name = 'contact'
) THEN CREATE TABLE public.contact (
    id SERIAL PRIMARY KEY,
    first_name TEXT,
    last_name TEXT,
    email TEXT,
    phone TEXT,
    information TEXT,
    user_id INTEGER NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users(id)
);
ALTER TABLE public.media
ADD CONSTRAINT fk_contact FOREIGN KEY(shared_id) REFERENCES contact(id);
END IF;
END $$;